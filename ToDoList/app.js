document.addEventListener("DOMContentLoaded", function(event){

	//jQuery UI tooltip with entry style
	$(document).tooltip({
		items: ".entry"
	});

	//fadeIn and fadeOut functions for adding and deleting a task
	function fadeIn(el, time) {
 		el.style.opacity = 0;

		let last = new Date().getTime();
		let tick = function() {
			el.style.opacity = +el.style.opacity + (new Date() - last) / time;
    		last = new Date().getTime();

    		if (+el.style.opacity < 1) {
      			(window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
    		}
  		};
  		tick();
	}

	function fadeOut(el, time) {
 		el.style.opacity = 1;

		let last = new Date().getTime();
		let tick = function() {
			el.style.opacity = +el.style.opacity - (new Date() - last) / time;
    		last = new Date().getTime();

    		if (+el.style.opacity > 0) {
      			(window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
    		}
    		else el.remove();
  		};
  		tick();
	}

	//capitalize first letter of task
	function capitalize(s)
	{
    	return s[0].toUpperCase() + s.slice(1);
	}

	//adding new task to To do table
	function addTask(){
		const taskInput = document.querySelector(".taskInput");

		if(taskInput.value!=""){
			const taskValue = taskInput.value;
			const task = document.createElement("div");

			task.classList.add("task", "ui-state-default");
			task.setAttribute("title", "Drag task to another table.");
			task.innerHTML = "<span class='delete' title='Delete task.'>&#10008;</span>" + "<span class='addDescription' title='Add details to your task.'>&#9662;</span>" + capitalize(taskValue);

			const div = document.querySelector(".tasks");
			div.appendChild(task);

			fadeIn(task, 300);

			document.querySelector('.taskInput').value = "";
		}
	}

	//making all tasks sortable in all tables
	$(".sortable").sortable({
     	connectWith: ".sortable"
    }).disableSelection();

	//functions for adding description to a task
	function descriptionForm(el){
		const descriptionForm = document.createElement("div");
		descriptionForm.classList.add("descriptionForm");
		descriptionForm.innerHTML = "<p>Add task description and press OK.</p>" + "<textarea class='descriptionInput' rows='4' cols='50' placeholder='Describe task...'></textarea>" + "<button class='okButton'>OK</button>";

		const container = document.querySelector(".container");
		container.appendChild(descriptionForm);

		document.querySelector(".okButton").addEventListener("click", function(){
			addDescription(el);
			descriptionForm.remove();
		});
	}

	function addDescription(el){
		inputValue = document.querySelector(".descriptionInput").value;

		if(inputValue!==""){
			el.classList.add("hasDescription");

			const description = document.createElement("p");
			description.classList.add("description");
			description.innerText = capitalize(inputValue);

			const div = el;
			div.appendChild(description);

			el.querySelector(".addDescription").remove();
			el.innerHTML += "<span class='addedDescription showAdded'>&#9662;</span>"
		}
	}

	//adding tasks on button click or on Enter pressed
	document.querySelector('button').addEventListener("click", function(){
		addTask();
	});

	document.querySelector(".taskInput").addEventListener("keyup", function(event){
		event.preventDefault();
		if (event.keyCode === 13) {
			addTask();
		}
	});

	//task events
	const tasks = document.querySelectorAll(".tasks");
	let i;
	for (i=0; i<tasks.length; i++){
		tasks[i].onclick = function(e){
			const event = e;
			const target = event.target; 
       		if(target.className === "delete") { 
        		fadeOut(target.parentElement, 300);
        		return false;
			}
			if(target.className === "addDescription") {
				descriptionForm(target.parentElement);
				return false;
			}
		}
		tasks[i].onmouseover = function(e){ 
			const event = e;
			const target = event.target; 
			if(target.className === "task ui-state-default hasDescription"){
				if(target.children[2].className === "addedDescription showAdded") target.querySelector(".addedDescription.showAdded").classList.remove("showAdded");
				target.children[1].classList.add("descriptionShow");
			}
		}
		tasks[i].onmouseout = function(e){ 
			const event = e;
			const target = event.target; 
			if(target.className === "task ui-state-default hasDescription" || target.className === "description descriptionShow"){
				target.querySelector(".addedDescription").classList.add("showAdded");
				target.children[1].classList.remove("descriptionShow");
			}
		}
	}

});